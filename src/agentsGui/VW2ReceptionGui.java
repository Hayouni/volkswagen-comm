package agentsGui;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import agents.VW2Reception;
import jade.gui.GuiEvent;
import main.DeliveryChoice;
import javax.swing.JTextArea;
import java.awt.Color;
import java.awt.event.ActionListener;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.SystemColor;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JSeparator;
import javax.swing.JDesktopPane;
import org.eclipse.wb.swing.FocusTraversalOnArray;
import java.awt.Component;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;
import java.awt.Toolkit;

public class VW2ReceptionGui {

	private int VW1partsnum;
	private int VW2partsnum;
	private int VW3partsnum;

	private JFrame frame;
	private JTextField PartsRef;
	private VW2Reception VW2ReceptionAgent;
	private JTextArea InformationMesage = new JTextArea();
	private JDesktopPane desktopPane_1 = new JDesktopPane();
	private JTextArea Workassign = new JTextArea();
	private JTextArea orderdetail = new JTextArea();
	private JTextField Timelabel;
	private JTextField Numberofparts;
	private JDesktopPane desktopPane_3 = new JDesktopPane();
	private JButton SubmittBtn2 = new JButton("Submit");
	JTextArea wshop1 = new JTextArea();
	JTextArea wshop2 = new JTextArea();
	JTextArea wshop3 = new JTextArea();

	DeliveryChoice[] DelivChoice = new DeliveryChoice[3];

	public VW2Reception getVW2ReceptionAgent() {
		return VW2ReceptionAgent;
	}

	public void setVW2ReceptionAgent(VW2Reception vW2ReceptionAgent) {
		VW2ReceptionAgent = vW2ReceptionAgent;
	}

	public VW2ReceptionGui() {
		initialize();

	}

	public void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage("C:\\Users\\Me\\Desktop\\nw.jpg"));
		frame.setBounds(100, 100, 1179, 777);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setVisible(true);

		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBackground(SystemColor.control);
		lblNewLabel.setIcon(new ImageIcon("C:\\Users\\Me\\Desktop\\nw.jpg"));
		lblNewLabel.setBounds(106, 24, 100, 93);
		frame.getContentPane().add(lblNewLabel);

		JLabel lblNewLabel_1 = new JLabel("Northland Volkswagen Repair Service");
		lblNewLabel_1.setForeground(SystemColor.textHighlight);
		lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblNewLabel_1.setBounds(366, 46, 364, 48);
		frame.getContentPane().add(lblNewLabel_1);

		JSeparator separator = new JSeparator();
		separator.setBounds(29, 11, 1109, 2);
		frame.getContentPane().add(separator);

		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(29, 128, 1109, 13);
		frame.getContentPane().add(separator_1);

		Timelabel = new JTextField();
		Timelabel.setHorizontalAlignment(SwingConstants.CENTER);
		Timelabel.setBounds(784, 45, 341, 48);
		frame.getContentPane().add(Timelabel);
		Timelabel.setFont(new Font("MS PGothic", Font.BOLD, 20));
		Timelabel.setBackground(new Color(169, 169, 169));
		Timelabel.setColumns(10);

		JDesktopPane desktopPane_2 = new JDesktopPane();
		desktopPane_2.setForeground(new Color(0, 255, 0));
		desktopPane_2.setBorder(new LineBorder(SystemColor.controlShadow, 3, true));
		desktopPane_2.setBackground(SystemColor.inactiveCaptionBorder);
		desktopPane_2.setBounds(50, 152, 1075, 578);
		frame.getContentPane().add(desktopPane_2);

		JLabel lblWorkshop = new JLabel("Workshop 1");
		lblWorkshop.setBounds(561, 349, 78, 23);
		desktopPane_2.add(lblWorkshop);

		JSeparator separator_2 = new JSeparator();
		separator_2.setBounds(551, 335, 495, 13);
		desktopPane_2.add(separator_2);

		JLabel lblRepairService = new JLabel("Repair Service");
		lblRepairService.setForeground(Color.BLACK);
		lblRepairService.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblRepairService.setBounds(759, 304, 364, 29);
		desktopPane_2.add(lblRepairService);

		JLabel label = new JLabel("Workshop 1");
		label.setBounds(759, 349, 89, 23);
		desktopPane_2.add(label);

		JLabel label_1 = new JLabel("Workshop 1");
		label_1.setBounds(955, 349, 94, 23);
		desktopPane_2.add(label_1);

		desktopPane_3.setBorder(new LineBorder(SystemColor.controlShadow, 3, true));
		desktopPane_3.setBackground(SystemColor.inactiveCaptionBorder);
		desktopPane_3.setBounds(31, 323, 483, 244);
		desktopPane_2.add(desktopPane_3);
		orderdetail.setBounds(40, 23, 403, 92);
		desktopPane_3.add(orderdetail);

		orderdetail.setBackground(new Color(0, 0, 0));
		orderdetail.setForeground(new Color(0, 255, 0));

		JButton btnOrder = new JButton("Order");
		btnOrder.setBounds(264, 138, 138, 49);
		desktopPane_3.add(btnOrder);

		JButton btnNewButton = new JButton("Order+assign");
		btnNewButton.setBounds(100, 139, 130, 49);
		desktopPane_3.add(btnNewButton);
		btnNewButton.setFont(new Font("Lucida Console", Font.BOLD, 11));

		JSeparator separator_3 = new JSeparator();
		separator_3.setBounds(26, 220, 439, 13);
		desktopPane_3.add(separator_3);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(33, 56, 405, 186);
		desktopPane_2.add(scrollPane);
		scrollPane.setViewportView(InformationMesage);
		InformationMesage.setBackground(new Color(0, 0, 0));
		frame.getContentPane().setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[] { SubmittBtn2,
				PartsRef, InformationMesage, lblNewLabel, lblNewLabel_1, separator, separator_1 }));

		JLabel lblEnterThePart = new JLabel("Enter the part reference number :");
		lblEnterThePart.setBounds(23, 22, 197, 23);
		desktopPane_2.add(lblEnterThePart);

		PartsRef = new JTextField();
		PartsRef.setBounds(218, 23, 136, 20);
		desktopPane_2.add(PartsRef);
		PartsRef.setColumns(10);

		SubmittBtn2.setBounds(364, 22, 89, 23);
		desktopPane_2.add(SubmittBtn2);
		desktopPane_1.setBounds(529, 22, 520, 244);
		desktopPane_2.add(desktopPane_1);
		desktopPane_1.setBorder(new LineBorder(SystemColor.controlShadow, 3, true));
		desktopPane_1.setBackground(SystemColor.inactiveCaptionBorder);
		Workassign.setForeground(new Color(0, 255, 0));
		Workassign.setBackground(new Color(0, 0, 0));

		Workassign.setBounds(72, 58, 362, 157);
		desktopPane_1.add(Workassign);

		JLabel lblEnterTheNumber = new JLabel("Enter the number of parts needed :");
		lblEnterTheNumber.setBounds(21, 24, 197, 23);
		desktopPane_1.add(lblEnterTheNumber);

		Numberofparts = new JTextField();
		Numberofparts.setBounds(228, 27, 136, 20);
		desktopPane_1.add(Numberofparts);
		Numberofparts.setEditable(false);
		Numberofparts.setColumns(10);

		JButton PartsNumSubBtn = new JButton("Submit");
		PartsNumSubBtn.setBounds(374, 24, 89, 23);
		desktopPane_1.add(PartsNumSubBtn);
		wshop1.setForeground(new Color(0, 255, 0));

		wshop1.setBackground(Color.BLACK);
		wshop1.setBounds(524, 383, 178, 184);
		desktopPane_2.add(wshop1);
		wshop3.setForeground(new Color(0, 255, 0));

		wshop3.setBackground(Color.BLACK);
		wshop3.setBounds(892, 383, 173, 184);
		desktopPane_2.add(wshop3);

		JSeparator separator_5 = new JSeparator();
		separator_5.setBounds(52, 280, 946, 13);
		desktopPane_2.add(separator_5);
		wshop2.setForeground(new Color(0, 255, 0));

		wshop2.setBackground(Color.BLACK);
		wshop2.setBounds(712, 383, 170, 184);
		desktopPane_2.add(wshop2);
		PartsNumSubBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int partnum = Integer.parseInt(Numberofparts.getText());
				GuiEvent gev2 = new GuiEvent(this, 2);
				gev2.addParameter(partnum);
				VW2ReceptionAgent.onGuiEvent(gev2);
				desktopPane_3.setBackground(Color.LIGHT_GRAY);

			}
		});
		SubmittBtn2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String partref = PartsRef.getText();
				GuiEvent gev = new GuiEvent(this, 1);
				gev.addParameter(partref);
				VW2ReceptionAgent.onGuiEvent(gev);
				orderdetail.setText("");
				InformationMesage.setText("");
				Workassign.setText("");
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				GuiEvent gev = new GuiEvent(this, 3);
				gev.addParameter(DelivChoice);
				VW2ReceptionAgent.onGuiEvent(gev);

			}
		});
		btnOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				GuiEvent gev4 = new GuiEvent(this, 4);
				gev4.addParameter(DelivChoice);
				VW2ReceptionAgent.onGuiEvent(gev4);
				Ordermsg("Parts Ordered");
			}
		});

	}

	public void showmsg(String msg) {
		InformationMesage.setBackground(Color.YELLOW);
		desktopPane_1.setBackground(Color.LIGHT_GRAY);
		Numberofparts.setEditable(true);
		InformationMesage.append(msg + "\n");

	}

	// this method receives a report from all the other repair shops and decides
	// where to order the parts from
	public void partsassign() {
		int requiredpartsnum = Integer.parseInt(Numberofparts.getText());
		String ref = PartsRef.getText();

		if (VW2partsnum >= requiredpartsnum) {

			Workassign.append("Parts available in local magazine" + "\n");
			Ordermsg("Parts required available in local Magazine" + "\n");
			Ordermsg("Click {Order+Assign} to order parts and assign work task" + "\n");
			DeliveryChoice dest1 = new DeliveryChoice("VW2Magazine", requiredpartsnum, ref, "VW2");
			DeliveryChoice dest2 = new DeliveryChoice(null, 0, null, null);
			DeliveryChoice dest3 = new DeliveryChoice(null, 0, null, null);
			DelivChoice[0] = dest1;
			DelivChoice[1] = dest2;
			DelivChoice[2] = dest3;

		} else if ((VW2partsnum < requiredpartsnum) && (VW1partsnum >= requiredpartsnum)) {

			Workassign.append("Parts available in VW1 magazine" + "\n");
			Ordermsg("Parts will be requested form VW2 Magazine" + "\n");
			Ordermsg("Click {Order+Assign} to order parts and assign work task" + "\n");
			Ordermsg("Click {Order} to order parts from VW2" + "\n");
			DeliveryChoice dest1 = new DeliveryChoice("VW1Reception", requiredpartsnum, ref, "VW2");
			DeliveryChoice dest2 = new DeliveryChoice(null, 0, null, null);
			DeliveryChoice dest3 = new DeliveryChoice(null, 0, null, null);
			DelivChoice[0] = dest1;
			DelivChoice[1] = dest2;
			DelivChoice[2] = dest3;
		} else if ((VW2partsnum < requiredpartsnum) && (VW1partsnum < requiredpartsnum)
				&& (VW3partsnum >= requiredpartsnum)) {

			Workassign.append("Parts available in VW3 magazine" + "\n");
			Ordermsg("Parts will be requested form VW3 Magazine" + "\n");
			Ordermsg("Click {Order+Assign} to order parts and assign work task" + "\n");
			Ordermsg("Click {Order} to order parts from VW3" + "\n");
			DeliveryChoice dest1 = new DeliveryChoice("VW3Reception", requiredpartsnum, ref, "VW2");
			DeliveryChoice dest2 = new DeliveryChoice(null, 0, null, null);
			DeliveryChoice dest3 = new DeliveryChoice(null, 0, null, null);
			DelivChoice[0] = dest1;
			DelivChoice[1] = dest2;
			DelivChoice[2] = dest3;
		} else if ((VW2partsnum < requiredpartsnum) && (VW1partsnum < requiredpartsnum)
				&& (VW3partsnum < requiredpartsnum) && (VW1partsnum + VW2partsnum >= requiredpartsnum)) {

			Workassign.append("Parts available in local magazine and VW1" + "\n");
			Ordermsg("Parts will be requested form VW1 Magazine and local Magazine" + "\n");
			Ordermsg("Click {Order+Assign} to order parts and assign work task" + "\n");
			Ordermsg("Click {Order} to order parts from VW2" + "\n");
			DeliveryChoice dest1 = new DeliveryChoice("VW2Magazine", VW2partsnum, ref, "VW2");
			DeliveryChoice dest2 = new DeliveryChoice("VW1Reception", requiredpartsnum - VW2partsnum, ref, "VW2");
			DeliveryChoice dest3 = new DeliveryChoice(null, 0, null, null);
			DelivChoice[0] = dest1;
			DelivChoice[1] = dest2;
			DelivChoice[2] = dest3;
		} else if ((VW2partsnum < requiredpartsnum) && (VW1partsnum < requiredpartsnum)
				&& (VW3partsnum < requiredpartsnum) && (VW2partsnum + VW3partsnum >= requiredpartsnum)) {

			Workassign.append("Parts available in local magazine and VW3" + "\n");
			Ordermsg("Parts will be requested form VW3 Magazine and local Magazine" + "\n");
			Ordermsg("Click {Order+Assign} to order parts and assign work task" + "\n");
			Ordermsg("Click {Order} to order parts from VW3" + "\n");
			DeliveryChoice dest1 = new DeliveryChoice("VW2Magazine", VW2partsnum, ref, "VW2");
			DeliveryChoice dest2 = new DeliveryChoice("VW3Reception", requiredpartsnum - VW2partsnum, ref, "VW2");
			DeliveryChoice dest3 = new DeliveryChoice(null, 0, null, null);
			DelivChoice[0] = dest1;
			DelivChoice[1] = dest2;
			DelivChoice[2] = dest3;
		} else if ((VW1partsnum < requiredpartsnum) && (VW2partsnum < requiredpartsnum)
				&& (VW3partsnum < requiredpartsnum) && (VW1partsnum + VW2partsnum + VW3partsnum >= requiredpartsnum)) {

			Workassign.append("Parts available in local magazine, VW1 and VW3" + "\n");
			Ordermsg("Parts will be requested form VW3,VW2 and local Magazine" + "\n");
			Ordermsg("Click {Order+Assign} to order parts and assign work task" + "\n");
			Ordermsg("Click {Order} to order parts from VW2" + "\n");
			DeliveryChoice dest1 = new DeliveryChoice("VW2Magazine", VW2partsnum, ref, "VW1");
			DeliveryChoice dest2 = new DeliveryChoice("VW1Reception", VW1partsnum, ref, "VW1");
			DeliveryChoice dest3 = new DeliveryChoice("VW3Reception", requiredpartsnum - VW1partsnum - VW2partsnum, ref,
					"VW2");
			DelivChoice[0] = dest1;
			DelivChoice[1] = dest2;
			DelivChoice[2] = dest3;

		} else {

			Workassign.append("Parts not available" + "\n");
		}

	}

	// shows the real time on the time label
	public void showtime() {
		Date date = new Date();
		Timelabel.setText(date.toString());
	}

	// this methods prints to a text box the details of order
	public void Ordermsg(String order) {
		if (orderdetail.getText().trim() == "") {
			orderdetail.setText(order);
		} else {
			orderdetail.append(order);
		}
	}

	public void getnum(int aa, int bb, int cc) {
		VW1partsnum = aa;
		VW2partsnum = bb;
		VW3partsnum = cc;

	}

	// this method prints to the workshops the detailed tasks
	public void workshop1tasks(int txtarea, String msg) {

		switch (txtarea) {
		case 1:
			wshop1.append(">" + msg + "\n");
			break;
		case 2:
			wshop2.append(">" + msg + "\n");
			break;
		case 3:
			wshop3.append(">" + msg + "\n");
			break;

		}

	}

	// this method clears the workshop text area
	public void Clearworkshoparea(int txtarea) {

		switch (txtarea) {
		case 1:
			wshop1.setText("");
			break;
		case 2:
			wshop2.setText("");
			break;
		case 3:
			wshop3.setText("");
			break;

		}

	}
}
