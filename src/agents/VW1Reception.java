package agents;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import agentsGui.VW1ReceptionGui;
import jade.core.AID;

import jade.core.Location;
import jade.core.behaviours.CyclicBehaviour;

import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import main.DeliveryChoice;

public class VW1Reception extends GuiAgent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int VW1partsnum = 0;
	int VW2partsnum = 0;
	int VW3partsnum = 0;
	private VW1ReceptionGui gui;
	int workshopindex = 0;
	String workshop = "";
	int dbcount = 0;
	int minnum = 0;
	int count1 = 0;
	int count2 = 0;
	int count3 = 0;
	DeliveryChoice[] DelivChoice = new DeliveryChoice[3];

	@Override
	protected void setup() {

		gui = new VW1ReceptionGui();
		gui.setVW1ReceptionAgent(this);
		System.out.println("VW1Reception agent deployed :" + this.getAID().getName());
		WorkAssign();
// add cyclic behavior
		addBehaviour(new CyclicBehaviour() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void action() {

				// define the message templates
				gui.showtime();
				MessageTemplate reportreceptiontemplate = MessageTemplate.and(
						MessageTemplate.MatchPerformative(ACLMessage.CONFIRM),
						MessageTemplate.MatchOntology("inventory-report"));
				MessageTemplate reportrequesttemplate = MessageTemplate.and(
						MessageTemplate.MatchPerformative(ACLMessage.QUERY_IF),
						MessageTemplate.MatchOntology("inventory-check"));
				MessageTemplate locmagtemplate = MessageTemplate.and(
						MessageTemplate.MatchPerformative(ACLMessage.CONFIRM),
						MessageTemplate.MatchOntology("local-inventory-report"));
				MessageTemplate DeliveryReqtemplate = MessageTemplate.and(
						MessageTemplate.MatchPerformative(ACLMessage.REQUEST_WHENEVER),
						MessageTemplate.MatchOntology("Delivery-Request"));
				ACLMessage aclMessage = receive(reportreceptiontemplate);
				ACLMessage repreqMessage = receive(reportrequesttemplate);
				ACLMessage localmagMessage = receive(locmagtemplate);
				ACLMessage DeliveryRequestMessage = receive(DeliveryReqtemplate);

				// if message received
				if (aclMessage != null) {
					System.out.println("VW1:report recieved");

					gui.showmsg("sender : " + aclMessage.getSender().getLocalName());
					gui.showmsg("Topic : " + aclMessage.getOntology());
					//gui.showmsg("Parts Availability : " + ACLMessage.getPerformative(aclMessage.getPerformative()));
					gui.showmsg("Quantity : " + aclMessage.getContent());
					gui.showmsg("----------------------------------------------");

					switch (aclMessage.getSender().getLocalName()) {
					case "VW1Magazine":
						VW1partsnum = Integer.parseInt(aclMessage.getContent());

						break;
					case "VW2Reception":
						VW2partsnum = Integer.parseInt(aclMessage.getContent());
						break;
					case "VW3Reception":
						VW3partsnum = Integer.parseInt(aclMessage.getContent());
						break;

					default:
						break;
					}

					gui.getnum(VW1partsnum, VW2partsnum, VW3partsnum);

				} else {
					block();
				}
				// inventory check request
				if (repreqMessage != null) {
					gui.showmsg("Inventory check requested by  : " + repreqMessage.getSender().getLocalName());
					ACLMessage locmagMessage = new ACLMessage(ACLMessage.REQUEST);
					locmagMessage.addReceiver(new AID("VW1Magazine", AID.ISLOCALNAME));
					locmagMessage.setContent(repreqMessage.getContent());
					locmagMessage.setOntology("inventory-check");
					locmagMessage.setProtocol(repreqMessage.getSender().getLocalName());
					send(locmagMessage);

				}
				// local inventory report
				if (localmagMessage != null) {

					ACLMessage VW1respMessage = new ACLMessage(ACLMessage.CONFIRM);
					VW1respMessage.addReceiver(new AID(localmagMessage.getProtocol(), AID.ISLOCALNAME));
					VW1respMessage.setContent(localmagMessage.getContent());
					VW1respMessage.setOntology("inventory-report");
					send(VW1respMessage);
					gui.showmsg("VW1:Response sent to   : " + localmagMessage.getProtocol());

				}
				// delivery request received
				if (DeliveryRequestMessage != null) {
					gui.showmsg("-------------------------------");
					gui.showmsg("VW1:Delivery Request received from   : "
							+ DeliveryRequestMessage.getSender().getLocalName());
					gui.showmsg("VW1:content  : " + DeliveryRequestMessage.getContent());
					Passdeliverymsg(DeliveryRequestMessage.getContent(),
							Integer.parseInt(DeliveryRequestMessage.getUserDefinedParameter("Quantity")),
							DeliveryRequestMessage.getUserDefinedParameter("reference"));

				}

			}

		});

	}

	@Override
	// take down this agent
	protected void takeDown() {
		System.out.println(this.getAID().getName() + "is destroyed");
	}

	@Override
	// add an onGui event listener
	public void onGuiEvent(GuiEvent ev) {
		switch (ev.getType()) {
		// button submit after entering the part reference number is clicked
		case 1:
			System.out.println("VW1: request sent to magazine");
			ACLMessage aclMessage = new ACLMessage(ACLMessage.QUERY_IF);
			String ref = (String) ev.getParameter(0);

			aclMessage.addReceiver(new AID("VW1Magazine", AID.ISLOCALNAME));
			aclMessage.setContent(ref);
			aclMessage.setOntology("inventory-check");
			aclMessage.addReceiver(new AID("VW2Reception", AID.ISLOCALNAME));
			aclMessage.addReceiver(new AID("VW3Reception", AID.ISLOCALNAME));
			send(aclMessage);

			break;
		case 2:
			// after entering the number of parts and clicking submit button
			gui.partsassign();

			break;
		case 3:
			// in case order+assign button is clicked
			workshop = WorkAssign();

			switch (workshop) {
			case "workshop1":
				dbcount = count1;

				break;
			case "workshop2":
				dbcount = count2;

				break;
			case "workshop3":
				dbcount = count3;

				break;

			default:
				break;
			}
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd");
			LocalDate localDate = LocalDate.now();
			String workref = "VW" + (dbcount + 1) + "/" + workshop + "@" + dtf.format(localDate) + "";
			UpdateWorkDb(workref, WorkAssign());
			gui.Ordermsg("  Parts Ordered" + "\n");
			gui.Ordermsg("  task ref " + workref + " assigned to " + workshop + "\n");
			gui.Clearworkshoparea(1);
			gui.Clearworkshoparea(2);
			gui.Clearworkshoparea(3);
			WorkAssign();
			DelivChoice = (DeliveryChoice[]) ev.getParameter(0);
			SendDeliveryRequest(DelivChoice);
			Sendrepairassignement("  task ref " + workref + " assigned to " + workshop + "\n");
			break;
		case 4:
			// in case order button is clicked
			DeliveryChoice[] DeliveryChoice = (DeliveryChoice[]) ev.getParameter(0);
			SendDeliveryRequest(DeliveryChoice);

			break;

		default:
			break;
		}

	}

	// this method consults the database of work assigned and decide which workshop
	// will be assigned the next task
	public String WorkAssign() {
		String workref = "0";
		String workref2 = "0";
		String workref3 = "0";
		String retval = "0";
		count1 = 0;
		count2 = 0;
		count3 = 0;

		try {

			Connection con = DriverManager.getConnection("jdbc:mysql://localhost/VW1", "root", "");
			PreparedStatement statement1 = con.prepareStatement("SELECT REF FROM workshop1 WHERE NOT status;");
			ResultSet result = statement1.executeQuery();
			PreparedStatement statement2 = con.prepareStatement("SELECT REF FROM workshop2 WHERE NOT status;");
			ResultSet result2 = statement2.executeQuery();
			PreparedStatement statement3 = con.prepareStatement("SELECT REF FROM workshop3 WHERE NOT status;");
			ResultSet result3 = statement3.executeQuery();

			while (result.next()) {
				workref = result.getString("REF");
				count1++;
				gui.workshop1tasks(1, workref);

			}

			while (result2.next()) {
				workref2 = result2.getString("REF");
				count2++;
				gui.workshop1tasks(2, workref2);

			}

			while (result3.next()) {
				workref3 = result3.getString("REF");
				count3++;
				gui.workshop1tasks(3, workref3);

			}

			minnum = Math.min(Math.min(count1, count2), count3);

		} catch (Exception e) {

			e.printStackTrace();
		}

		System.out.println(count1 + "  " + count2 + "  " + count3 + " min " + minnum);

		if (minnum == count1) {
			retval = "workshop1";
		} else if (minnum == count2) {
			retval = "workshop2";
		} else if (minnum == count3) {
			retval = "workshop3";
		}

		System.out.println(retval);

		return retval;
	}

	// this work updates the work database after assigning a task to a workshop
	public void UpdateWorkDb(String workref, String workshop) {

		try {
			Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost/VW1", "root", "");
			PreparedStatement statement3 = con2.prepareStatement(
					"INSERT INTO " + workshop + " (REF, status) VALUES (\"" + workref + "\",false) ;");
			statement3.execute();

		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	// this method sends a delivery request to a specific agent with a specific
	// reference number and quantity
	public void SendDeliveryRequest(DeliveryChoice[] DelivChoice) {

		for (DeliveryChoice x : DelivChoice) {
			if (x.destination != null) {

				ACLMessage aclMessage = new ACLMessage(ACLMessage.REQUEST_WHENEVER);
				aclMessage.addReceiver(new AID(x.destination, AID.ISLOCALNAME));
				aclMessage.setContent("DeliveryRequest: [parts ref#: " + x.ref + ", Quantity: " + x.quantity
						+ " ,Requester:" + x.Sender + "]");

				aclMessage.setOntology("Delivery-Request");
				aclMessage.addUserDefinedParameter("Quantity", String.valueOf(x.quantity));
				aclMessage.addUserDefinedParameter("reference", x.ref);

				try {
					send(aclMessage);
					System.out.println("VW1: request sent to " + x.destination);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	// when vw1 receives a delivery request it passes the request to the local magazine 
	public void Passdeliverymsg(String msg, int quantity, String ref) {

		ACLMessage aclMessage = new ACLMessage(ACLMessage.REQUEST_WHENEVER);
		aclMessage.addReceiver(new AID("VW1Magazine", AID.ISLOCALNAME));
		aclMessage.setContent(msg);

		aclMessage.setOntology("Delivery-Request");
		aclMessage.addUserDefinedParameter("Quantity", String.valueOf(quantity));
		aclMessage.addUserDefinedParameter("reference", ref);

		try {
			send(aclMessage);
			System.out.println("VW1: request sent to local magazine");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	public void Sendrepairassignement(String msg)
	{
		ACLMessage aclMessage = new ACLMessage(ACLMessage.PROPOSE);
		aclMessage.addReceiver(new AID("VW1Repair", AID.ISLOCALNAME));
		aclMessage.setContent(msg);
		aclMessage.setOntology("Work-assign");
		try {
			send(aclMessage);
			System.out.println("VW1: task work assigned to VW1Repair");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
