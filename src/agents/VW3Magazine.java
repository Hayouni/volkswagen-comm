package agents;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;



public class VW3Magazine extends Agent{
	
	public Boolean sendResponse=false;
	
	
	
	
	@Override
	protected void setup() {
	
	
		System.out.println("VW3Magazine agent deployed :"+this.getAID().getName());
		
		
		
		addBehaviour(new CyclicBehaviour() {
			
			@Override
			public void action() { 
				MessageTemplate reportrequesttemplate = MessageTemplate.and(
						MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
						MessageTemplate.MatchOntology("inventory-check"));
				MessageTemplate reportreceptiontemplate = MessageTemplate.and(
						MessageTemplate.MatchPerformative(ACLMessage.QUERY_IF),
						MessageTemplate.MatchOntology("inventory-check"));
				MessageTemplate DeliveryReqtemplate = MessageTemplate.MatchOntology("Delivery-Request");
				ACLMessage aclMessage = receive(reportreceptiontemplate);
				ACLMessage repreqMessage = receive(reportrequesttemplate);
				ACLMessage DeliveryRequestMessage = receive(DeliveryReqtemplate);
				if (aclMessage != null){
					
					System.out.println("VW3mag:message receive from "+aclMessage.getSender().getName() +" with content");
					String ref=aclMessage.getContent().toString();
					ACLMessage sendInventoryReport=new ACLMessage(ACLMessage.CONFIRM);
					sendInventoryReport.addReceiver(new AID("VW3Reception",AID.ISLOCALNAME));
					sendInventoryReport.setContent(Checkinventory(ref));
					sendInventoryReport.setOntology("inventory-report");
					send(sendInventoryReport);
					System.out.println("VW3Mag:Response sent back to VW2Reception  ");


				}
				else {
					block();
				}
                if (repreqMessage != null) {
					
					System.out.println("VW3MAg :message receive from "+repreqMessage.getSender().getName() +" with content");
					String ref=repreqMessage.getContent().toString();
					ACLMessage sendInventoryReport=new ACLMessage(ACLMessage.CONFIRM);
					sendInventoryReport.addReceiver(new AID("VW3Reception",AID.ISLOCALNAME));
					sendInventoryReport.setContent(Checkinventory(ref));
					sendInventoryReport.setOntology("local-inventory-report");
					sendInventoryReport.setProtocol(repreqMessage.getProtocol());
					send(sendInventoryReport);
					System.out.println("VW3mag:Response sent back to VW1Reception with protocol "+sendInventoryReport.getProtocol());


				}
            	if (DeliveryRequestMessage != null) {
    				
					System.out.println(" VW1Magazine: Delivery request received  "+ DeliveryRequestMessage.getContent());
					int quantity= Integer.parseInt(DeliveryRequestMessage.getUserDefinedParameter("Quantity"));
					String ref=DeliveryRequestMessage.getUserDefinedParameter("reference");
					//update the database after receiving the delivery request
					try {
						Connection con2 = DriverManager.getConnection("jdbc:mysql://localhost/VW3", "root", "");
						PreparedStatement statement3 = con2.prepareStatement("UPDATE magazinm SET QUANTITY=QUANTITY-"+quantity+" WHERE REF=\""+ref+"\" ;");
						statement3.execute();
						System.out.println("VW1Magazine: Inventory database updated");

					} catch (SQLException e) {
						e.printStackTrace();
					}

					
					

				}
				
			}
		});
		
	
	}

	
	public static String  Checkinventory(String ref) {
		
		String num="0";
		try {
			
			Connection con=DriverManager.getConnection("jdbc:mysql://localhost/VW3", "root", "");
			PreparedStatement statement=con.prepareStatement("SELECT QUANTITY FROM magazinm WHERE REF=\""+ref+"\";");
			ResultSet result = statement.executeQuery();
			while (result.next()) {
				num=result.getString("QUANTITY");
				System.out.println(num);
				
			}
			
		} catch (Exception e) {
		
			e.printStackTrace();
		}
		
		return num;
		

	}
	

	
}
