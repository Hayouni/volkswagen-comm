package agents;


import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class VW2Repair extends Agent{
	
	@Override
	protected void setup() {
		System.out.println("VW1Repairagent deployed :"+this.getAID().getName());
		
		addBehaviour(new CyclicBehaviour() {
			
			@Override
			public void action() {
				MessageTemplate reportreceptiontemplate = MessageTemplate.and(
						MessageTemplate.MatchPerformative(ACLMessage.PROPOSE),
						MessageTemplate.MatchOntology("Work-assign"));
				ACLMessage aclMessage = receive(reportreceptiontemplate);
				// if message received
				if (aclMessage != null) {
				System.out.println("VW1Repair :"+aclMessage.getContent());

				} else {
					block();
				}
				
			}
		});
		
	}

}
