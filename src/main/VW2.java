package main;

import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;

public class VW2 {

	public static void main(String[] args) {

		try {
			Runtime rt=Runtime.instance();
			ProfileImpl pc= new ProfileImpl(false);
			pc.setParameter(ProfileImpl.MAIN_HOST, "localhost");
			pc.setParameter(ProfileImpl.CONTAINER_NAME, "VW2");
		    //pc.setParameter(ProfileImpl.MAIN_PORT, "7778");
			AgentContainer ac=rt.createAgentContainer(pc);
			AgentController agentController=ac.createNewAgent("VW2Reception", "agents.VW2Reception", new Object[] {});
	        agentController.start();
	        AgentController agentController1=ac.createNewAgent("VW2Magazine", "agents.VW2Magazine", new Object[] {});
	        agentController1.start();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
