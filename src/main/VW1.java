package main;

import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;

public class VW1 {

	public static void main(String[] args) {

		try {
			Runtime rt=Runtime.instance();
			ProfileImpl pc= new ProfileImpl(false);
			pc.setParameter(ProfileImpl.MAIN_HOST, "DESKTOP-8F5DO3C");
			pc.setParameter(ProfileImpl.CONTAINER_NAME, "VW1");
		    //pc.setParameter(ProfileImpl.MAIN_PORT, "7778");
			AgentContainer ac=rt.createAgentContainer(pc);
			AgentController agentController=ac.createNewAgent("VW1Reception", "agents.VW1Reception", new Object[] {});
	        agentController.start();
	        AgentController agentController1=ac.createNewAgent("VW1Magazine", "agents.VW1Magazine", new Object[] {});
	        agentController1.start();
	        AgentController agentController2=ac.createNewAgent("VW1Repair", "agents.VW1Repair", new Object[] {});
	        agentController2.start();
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}

	}

}
