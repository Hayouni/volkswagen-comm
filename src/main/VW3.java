package main;

import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;

public class VW3 {

	public static void main(String[] args) {

		try {
			Runtime rt=Runtime.instance();
			ProfileImpl pc= new ProfileImpl(false);
			pc.setParameter(ProfileImpl.MAIN_HOST, "DESKTOP-8F5DO3C");
			pc.setParameter(ProfileImpl.CONTAINER_NAME, "VW3");
		    //pc.setParameter(ProfileImpl.MAIN_PORT, "7778");
			AgentContainer ac=rt.createAgentContainer(pc);
			AgentController agentController=ac.createNewAgent("VW3Reception", "agents.VW3Reception", new Object[] {"xml"});
	        agentController.start();
	        AgentController agentController1=ac.createNewAgent("VW3Magazine", "agents.VW3Magazine", new Object[] {"xml"});
	        agentController1.start();
	        
		} catch (ControllerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
